Als de oc import niet werkt kun je ook proberen om vanuit je lokale docker naar de je OpenShi(f)t repo te pushen. Disclaimer: ik doet dit even uit mijn hoofd, geen garantie dat er geen typefouten in zitten :)

Zorg dat je de image in de lokale docker hebt staan, anders even docker pull access.redhat... etc.

Eerst je API token ophalen uit OpenShift:

    $ oc whoami -t
    <token>

Vervolgens zorgen dat je met docker ingelogd ben op je OpenShift registry:

    $ docker login <url of ip van je openshift repo, bijv. 172.30.1.1:5000> -u blabla
    password is je <token>
Username maakt niet uit, gezien je je token gebruikt voor authenticatie.

Lokale image taggen naar je repo:

    $ docker tag 3scale-amp20/apicast-gateway:1.0 172.30.1.1:5000/3scalegateway/apicast-gateway:1.0
(eventueel kan je ook het imageId gebruiken ipv de naam. Bjiv. docker tag 132b7427a3b4 172.30.1.1:5000/3scalegateway/apicast-gateway:1.0)

Lokale image pushen naar OpenShift:
    
    $ docker push 172.30.1.1:5000/3scalegateway/apicast-gateway:1.0

Dit moet je wel voor iedere tag met de hand doen volgens mij.