# Python SolarPanel Monitoring

- [Python SolarPanel Monitoring](#python-solarpanel-monitoring)
    - [The Setup](#the-setup)
        - [Hardware](#hardware)
        - [Software used](#software-used)
        - [Docker containers](#docker-containers)
        - [The code](#the-code)

## The Setup

### Hardware

- 4 Raspberry pi's
- 4 SD cards, at least 8GB
- Any network swith
- OmnikSol 4k TL2 Converter with WiFi

### Software used

- [HypriotOS](https://blog.hypriot.com/) for pi's
- Python 2.7 (I know..)
- PyCharm or Intellij with the Python plugin

### Docker containers

I try to use as much Hypriot 'certified'  containers if possible, to maximize compatibility with the Pi's ARM processor.

- [Portainer](https://hub.docker.com/r/hypriot/rpi-portainer/)
- [InfluxDB](https://hub.docker.com/r/hypriot/rpi-influxdb/)
- [Graphana](https://hub.docker.com/r/fg2it/grafana-armhf/)
- [arm32v7/python base image](https://hub.docker.com/r/arm32v7/python/)

### The code

The code is located on my [personal BitBucket repo](https://bitbucket.org/DeGup/omnik-data-logger/src/master/)