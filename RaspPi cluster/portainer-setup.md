# Pi Setup

## Hypriot OS

Grap the Hypriot OS image from [their site](https://blog.hypriot.com/downloads/).

Use your favourite image writing tool to transfer the .iso over to your SD card. I use [Etcher](https://etcher.io/), which runs on Linx, Windows and Mac.

You can use the [how-to](https://blog.hypriot.com/getting-started-with-docker-and-linux-on-the-raspberry-pi/) on the Hypriot site if you run into issues.

Insert the card in the Pi and power up. Locate the ip of you pi using your preferred method.

You should now be able to ssh into the pi using the default username **pirate** and password **hypriot**.

For example:

````shell
ssh pirate@192.168.0.51
````

Repeat the setup for the three other pi's.

## Docker Swarm

I am using Docker Swarm to schedule my containers of all four pi's.

First off, check if you have docker running correctly by ssh'ing to your pi's and checking your docker version:

````shell
docker -v
````

If you do not have permissions to execute docker commands, either switch to root or add the pirate user to the docker group:

````shell
sudo usernmod -aG docker $USER
````

Login to the pi that should be the master node and make a note of it's ip address. Next,execute the **docker swarm init** command with our master node ip as MANAGER-IP:

````shell
docker swarm init --advertise-addr <MANAGER-IP>
````

This will setup the master and generate a join token and join command. This looks like:

````shell
    docker swarm join \
    --token SWMTKN-1-49nj1cmql0jkz5s954yi3oex3nedyz0fb0xx14ie39trti4wxv-8vxv8rssmk743ojnwacrr2e7c \
    192.168.0.51:2377
````

Make a note of this token and command, as we need it to join the other nodes to the master.

Ssh to the other pi's and execute the previous command to join them to the swarm.

You can check the state of the swarm by executing:

````shell
docker info
````

Ouput should look like:

````shell
Containers: 2
Running: 0
Paused: 0
Stopped: 2
  ...snip...
Swarm: active
  NodeID: dxn1zf6l61qsb1josjja83ngz
  Is Manager: true
  Managers: 1
  Nodes: 3
  ...snip...
````

**docker node ls** should give more info on the registered nodes.

## Portainer

We will use Portainer as a fron-end and management tool for our swarm. Setting it up is fairly easy.

You can either use the [hypriot image](https://hub.docker.com/r/hypriot/rpi-portainer/), or the [offical image](https://hub.docker.com/r/portainer/portainer/)

If using the offical image, make sure to use the **linux-arm64**
tag.

To run it, just execute:

````shell
docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock hypriot/rpi-portainer
````

### InfluxDB

### Graphana

#### InfluxDB Datasource
