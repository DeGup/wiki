Nope, docker swarm is next:
https://howchoo.com/g/njy4zdm3mwy/how-to-run-a-raspberry-pi-cluster-with-docker-swarm


    docker swarm join --token SWMTKN-1-0kgr5cvd0wsla4ce2j9vtp9s6mzi5aic5bycq6cq7lz2iv77ej-dq6m9ids6otlm2a2bti56i7a8 192.168.1.61:2377


----

New attempt, now using HypriotOS and the guide from https://blog.hypriot.com/post/setup-kubernetes-raspberry-pi-cluster/. Bit outdate, but should still do the trick.

Flash cards and run on each system:
 Username: pirate
 Password: hypriot

Based on raspbian, so still use the rasppi config to set hostname
No kernel and boot changes needed. Resize of the nodes not needed. All pre-setup is already incleaded in the image. Install kubeadm and call init.
Will use the flannel router this time around.


Setting hostname in reverse order to prevent dns issues on network
1. master: black-pearl
1. node1: red-pearl
1. node2: yellow-pearl
1. node3: green-pearl

Not going to make the master schedulable this time.

Blocks in kubeadm init:
https://github.com/kubernetes/kubeadm/issues/684

Downgrade kubeadm to 1.9.7-00 as a workaround
````
$ apt remove kubelet kubeadm kubectl

$ apt install kubelet=1.9.7-00 kubeadm=1.9.7-00 kubectl=1.9.7-00
````

Then init with prep for flannel:
````
$ kubeadm init --pod-network-cidr 10.244.0.0/16
````

kubeadm join --token 4bc71f.2c46b0ffd97b96fc 192.168.1.61:6443 --discovery-token-ca-cert-hash sha256:0aa96ca71cd2c5352e1619665dff17cdadd7ecc98b6cf01979307f9b4992ea49




Error on nodes:
Container runtime network not ready: NetworkReady=false reason:NetworkPluginNotReady message:docker: network plugin is not ready: cni config uninitialized

Export KUBECONFIG to use as local user.
````
$ sudo cp /etc/kubernetes/admin.conf $HOME/
$ sudo chown $(id -u):$(id -g) $HOME/admin.conf
$ export KUBECONFIG=$HOME/admin.conf
````
(Add the export line to ~/.bash_profile to persist after boot)

Use the flannel version as listed in the guide to avoid compat issues:
````
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.9.1/Documentation/kube-flannel.yml
````




-----

# Prep
## Get:

* Four Raspberry Pi 3's
* Four mircoSDHC  cards
* Four chargers
* Four small cat-5's
* A cheap sd card reader

## Setup:

1. Download Ubuntu for ARM (user en pass: ubuntu) - Wont work
1. Get Raspbian (pi, raspberry)
1. Write to SD with Etcher (Windows) or (using dd (unzip -p 2018-03-13-raspbian-stretch.zip | sudo dd of=/dev/sdX bs=4M conv=fsync))
1. Add an empty file called 'ssh' to the boot folder on the sd card to enable headless setup. ($ touch ssh)
1. ssh into the pi's and change password! ($ passwd)
1. Change hostname for ease of management. Use raspi-config to prevent changing it in multiple places:
````
$ sudo raspi-config
````
1. Resize root partition in raspi-config
````
$ sudo raspi-config
Advanced option -> Expand Filesystem
````

## Docker:

** Install some required packages first**

sudo apt update

sudo apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common

** Get the Docker signing key for packages**

curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -

** Add the Docker official repos**

echo "deb [arch=armhf] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list

** Install Docker**

sudo apt update

sudo apt install docker-ce

(source: https://withblue.ink/2017/12/31/yes-you-can-run-docker-on-raspbian.html)

## Testing docker:

$ sudo docker run --rm arm32v7/hello-world


## Setup Kubeadm
https://kubernetes.io/docs/setup/independent/install-kubeadm/

# Errors on kubeadm init:

CGROUPS_MEMORY: missing
        [WARNING SystemVerification]: docker version is greater than the most recently validated version. Docker version: 18.03.0-ce. Max validated version: 17.03
        [WARNING FileExisting-crictl]: crictl not found in system path
Suggestion: go get github.com/kubernetes-incubator/cri-tools/cmd/crictl
[preflight] Some fatal errors occurred:
        [ERROR SystemVerification]: missing cgroups: memory
        [ERROR Swap]: running with swap on is not supported. Please disable swap

## Fixing cgroups: memory
$ vim /boot/cmdline.txt
Add: cgroup_enable=memory cgroup_memory=1

## Disable swap
````bash
sudo dphys-swapfile swapoff
sudo dphys-swapfile uninstall
sudo update-rc.d dphys-swapfile remove
sudo reboot
````

## Create cluster on one node:
Need to be root!
````bash
$ sudo kubeadm init
````

Take note of the join command, need that to join the cluster:
````
kubeadm join 192.168.1.58:6443 --token cqalkh.gmrgty7uazphowla --discovery-token-ca-cert-hash sha256:6927b5900d4a198f71bbb55a7e5d8f5c11653f0c625324d186be0d4a56008a0c
````

## Move the admin.conf to it's location:
Either as root or local user:
````bash
$ sudo cp /etc/kubernetes/admin.conf $HOME/
$ sudo chown $(id -u):$(id -g) $HOME/admin.conf
$ export KUBECONFIG=$HOME/admin.conf
````
*This will make that user admin on the cluster!*

Check connection with a simple command:
````bash
$ kubectl version
Client Version: version.Info{Major:"1", Minor:"10", GitVersion:"v1.10.0", GitCommit:"fc32d2f3698e36b93322a3465f63a14e9f0eaead", GitTreeState:"clean", BuildDate:"2018-03-26T16:55:54Z", GoVersion:"go1.9.3", Compiler:"gc", Platform:"linux/arm"}
Server Version: version.Info{Major:"1", Minor:"10", GitVersion:"v1.10.1", GitCommit:"d4ab47518836c750f9949b9e0d387f20fb92260b", GitTreeState:"clean", BuildDate:"2018-04-12T14:14:26Z", GoVersion:"go1.9.3", Compiler:"gc", Platform:"linux/arm"}
````

## Setup pod network
Flannel of Weave Net
Going for Weave Net as I forgot to add the correct flags on init..

**Export Kube version:**
````bash
export kubever=$(kubectl version | base64 | tr -d '\n')
````
**apply the weave template**
````bash
$ kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$kubever"
serviceaccount "weave-net" created
clusterrole.rbac.authorization.k8s.io "weave-net" created
clusterrolebinding.rbac.authorization.k8s.io "weave-net" created
role.rbac.authorization.k8s.io "weave-net" created
rolebinding.rbac.authorization.k8s.io "weave-net" created
daemonset.extensions "weave-net" created

````

## Enable master scheduling
I want to be able to use all my pi's for pods, including the master. Need to deisable Master isolation:
````bash
$ kubectl taint nodes --all node-role.kubernetes.io/master-
node "raspberrypi1" untainted
````

Current state:
````bash
$ kubectl get pods --all-namespaces
kube-system   etcd-raspberrypi1                      1/1       Running   0          2m
kube-system   kube-apiserver-raspberrypi1            1/1       Running   0          2m
kube-system   kube-controller-manager-raspberrypi1   1/1       Running   0          3m
kube-system   kube-dns-686d6fb9c-d5v4d               3/3       Running   0          3m
kube-system   kube-proxy-dxrq7                       1/1       Running   0          3m
kube-system   kube-scheduler-raspberrypi1            1/1       Running   0          2m
kube-system   weave-net-rlqf6                        2/2       Running   0          1m
````

## Joining the cluster
**As root**, call the join command that was listed as output from the init.
````bash

$  sudo kubeadm join 192.168.1.58:6443 --token ... --discovery-token-ca-cert-hash ...

<truncated>
This node has joined the cluster:
* Certificate signing request was sent to master and a response
  was received.
* The Kubelet was informed of the new secure connection details.

Run 'kubectl get nodes' on the master to see this node join the cluster.
````

On our raspberrypi1 master:
````bash
NAME           STATUS     ROLES     AGE       VERSION
raspberrypi1   Ready      master    6m        v1.10.0
raspberrypi2   NotReady   <none>    1m        v1.10.0
````

On the p2..
````bash
Message from syslogd@raspberrypi2 at Apr 17 19:41:40 ...
 kernel:[ 1518.744076] Internal error: Oops: 80000007 [#1] SMP ARM
````

Master errors on join:
````
Message from syslogd@raspberrypi1 at Apr 17 19:44:46 ...
 kernel:[  172.262058] Internal error: Oops: 80000007 [#1] SMP ARM
````

On boot, error on the kubctl.service:
````
Warning: kubelet.service changed on disk. Run 'systemctl daemon-reload' to reload units.
````

On all nodes after join:
````
$ sudo systemctl daemon-reload
````

Everything seems to overhead and just die now..

Waiting for heatsinks

Still joined from other nodes

Error on all nodes:
'Unable to update cni config: No networks found in /etc/cni/net.d'

Trying to disable swap and and setting cgroups on nodes.

-----



https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/ (step 4/4, all should've joined)



https://blog.hypriot.com/post/run-docker-rpi3-with-wifi/

https://kubernetes.io/docs/setup/independent/install-kubeadm/

https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

https://kubernetes.io/docs/concepts/cluster-administration/addons/

https://github.com/kubernetes/dashboard#kubernetes-dashboard
