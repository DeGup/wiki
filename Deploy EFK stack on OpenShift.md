[The official docs](https://docs.openshift.com/container-platform/3.6/install_config/aggregate_logging.html#aggregate-logging-kibana) 

Note: we are going to use hostPath persistence for the moment, as the EFK stack is not compatible with NFS. You could setup NFS on the nodes to point to the hostPath mount we are going to use as a workaround.

## Label your nodes

Elastic search deploys three nodes by default. Make sure that each of these nodes is labelled with an unique identifier in your host file. We will use 'logging-es-node': '<number>' for this.
We used the infra nodes in the infratest cluster. Relevant part of our hosts file:


    node1 openshift_node_labels="{'region': 'infra','network': 'f3','environment':'ota','elasticsearch': 'true', 'logging-es-node': '1'}"
    node2 openshift_node_labels="{'region': 'infra','network': 'f3','environment':'ota','elasticsearch': 'true', 'logging-es-node': '2'}"
    node3 openshift_node_labels="{'region': 'infra','network': 'f3','environment':'ota','elasticsearch': 'true', 'logging-es-node': '3'}"


Note the 'elasticsearch' and  'logging-es-node' labels on each system, each with an incremental value, to annotate which elastic search node should land where. The 'elasticsearch': 'true' label is used to specify the nodes to use of all logging. 

## Create the storage folders on each host

Create the following folder on each of your elastic search nodes:
    
    # mkdir /usr/local/es-storage

And set the correct permissions:

    # chmod -R 770 /usr/local/es-storage
 
Double check that above steps are executed for each of the atomic hosts running a Elastic Search node. In our case, node1, node2 and node3.
## Run the openshift-logging playbook
Next up, run the playbook as usual:

    # ansible-playbook /usr/share/ansible/openshift-ansible/playbooks/byo/openshift-cluster/openshift-logging.yml

This will install all the needed components and accounts for an ephemeral logging sollution. This will setup fluentd pods on all nodes in the cluster, create three elastic search nodes, setup a elastic search curator and a kibana instance.

# Setup policies

In order to read and write from and to the host system, the eslastic search containers need to be run privileged.
Add the privileged role to the aggregate-logging-elasticsearch serviceaccount:


    $ oc adm policy add-scc-to-user privileged  \
       system:serviceaccount:logging:aggregated-logging-elasticsearch

Then scale down all elastic search deployments and add the privileged=true flag to the securityContext:

    $ for dc in $(oc get deploymentconfig --selector logging-infra=elasticsearch -o name); do
    oc scale $dc --replicas=0
    oc patch $dc \
       -p '{"spec":{"template":{"spec":{"containers":[{"name":"elasticsearch","securityContext":{"privileged": true}}]}}}}'
  done

## Set nodeselector
For this, you need to go into all elastic search deployment config manually and set a node selector to specify your pod to run on only one specific host.

### Either do:

    $ oc edit cd logging-es-<suffix>
And add the nodeSelector to you spec.template.spec:

    apiVersion: v1
    kind: DeploymentConfig
    spec:
      template:
        spec:
          nodeSelector:
            logging-es-node: "<your incrementing number>"

### Or patch the dc directly:
 
    $ oc patch dc logging-es-<suffix> \
       -p '{"spec":{"template":{"spec":{"nodeSelector":{"logging-es-node":"<your incrementing number>"}}}}}'

You will need to do this three times. Replace <your incrementing number> by the numbers in your hosts file. In this case we have three es nodes which will land on logging-es-node 1, 2 and 3.

## Set the hostPath and rollout
Last step is to patch the dc's, replacing the emptyDir volume with the actual hostPath:

    $ for dc in $(oc get deploymentconfig --selector logging-
     infra=elasticsearch -o name); do
        oc set volume $dc \
              --add --overwrite --name=elasticsearch-storage \
              --type=hostPath --path=/var/openshift/local/es-storage
        oc rollout latest $dc
        oc scale $dc --replicas=1
     done

You will likely see some error that a rollout/deployment is already happening. Ignore those. You can probably skip the oc rollout and/or oc scale commands, as either of them will trigger a new deploy.

## Resource requirements
The logging project will contain the following components with the given limits:



**Kibana **- Runs on one node - 256mb - no CPU limit set by default

**Elastic search** - By default: 3 nodes - 8GB per node - 1 cpu per node

**Fluentd **- Runs on all nodes in the cluster - 512mb per node - 100 milli cpu per node

**Curator **- Runs on one node - no RAM limit set by default - 100 milli cpu