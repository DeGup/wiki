# Running Multple Camel REST Servlets on a single EAP/Wildfly Instance

Consider two deployments with the following web.xml:

````xml

<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">

    <!-- XML file -->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>WEB-INF/applicationContext.xml</param-value>
    </context-param>

    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>

    <!--Camel servlet -->
    <servlet>
        <servlet-name>CamelServlet</servlet-name>
        <servlet-class>org.apache.camel.component.servlet.CamelHttpTransportServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <!-- Camel servlet mapping -->
    <servlet-mapping>
        <servlet-name>CamelServlet</servlet-name>
        <url-pattern>/*</url-pattern>
    </servlet-mapping>
</web-app>

````

Both deployments use the servlet named 'CamelServlet'. One deployment is configured for conext root 'one', the other runs on 'two'. Our jboss-web.xml looks like:

Deployment one:

````xml
<jboss-web>
    <context-root>/one</context-root>
</jboss-web>
````

Deployment two
````xml
<jboss-web>
    <context-root>/two</context-root>
</jboss-web>
````

Running one deployment will work and will nicely log:

````
11:53:46,753 INFO  [javax.enterprise.resource.webcontainer.jsf.config] (ServerService Thread Pool -- 83) Initializing Mojarra 2.2.13.SP5  for context '/one'
11:53:46,853 INFO  [org.apache.camel.component.servlet.CamelHttpTransportServlet] (ServerService Thread Pool -- 83) Initialized CamelHttpTransportServlet[name=CamelServlet, contextPath=/one]
11:53:46,854 INFO  [org.wildfly.extension.undertow] (ServerService Thread Pool -- 83) WFLYUT0021: Registered web context: '/one' for server 'default-server'
11:53:46,854 INFO  [org.wildfly.extension.camel] (ServerService Thread Pool -- 83) Add Camel endpoint: http://127.0.0.1:8080/one
````

However, trying to deploy our second deployment will throw an error:

````
"WFLYCTL0080: Failed services" => {"jboss.undertow.deployment.default-server.default-host./one" => "javax.servlet.ServletException: Duplicate ServletName detected: CamelServlet. Existing: CamelHttpTransportServlet[name=CamelServlet] This: CamelHttpTransportServlet[name=CamelServlet]. Its advised to use unique ServletName per Camel application.
    Caused by: javax.servlet.ServletException: Duplicate ServletName detected: CamelServlet. Existing: CamelHttpTransportServlet[name=CamelServlet] This: CamelHttpTransportServlet[name=CamelServlet]. Its advised to use unique ServletName per Camel application."}}
````

So, I lets change the camel ServletName for the second deployment. Let's call it SecondCamelServlet:

````xml
    <!--Camel servlet -->
    <servlet>
        <servlet-name>SecondCamelServlet</servlet-name>
        <servlet-class>org.apache.camel.component.servlet.CamelHttpTransportServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <!-- Camel servlet mapping -->
    <servlet-mapping>
        <servlet-name>SecondCamelServlet</servlet-name>
        <url-pattern>/*</url-pattern>
    </servlet-mapping>
````

On Camel 2.21 / EAP 7 or Wildfly 11, this will work just fine.

On an older version, lets say EAP 6.4, this will not work. Camel will still try to deploy to the Default Servlet named CamelServlet. There is a not-that-well documented setting on the restConfiguration what will allow you to bind to a specific Servlet:

```java
@Component
public class MyRouter extends RouteBuilder {

    @Override
    public void configure() {

        restConfiguration("servlet")
                .endpointProperty("servletName", "SecondCamelServlet");


        rest("/")
                .get().produces("application/json").to("direct:start");

        from("direct:start")
                .setBody(constant("{'message': 'Hello world from deployment one!'}"));
    }

}

```

This will also allow you the configure two servlet configs in one Camel deployment.
Consider this web.xml:

````xml

<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">

    <!-- XML file -->
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>WEB-INF/applicationContext.xml</param-value>
    </context-param>

    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>

    <!--Camel servlet -->
    <servlet>
        <servlet-name>ServletOne</servlet-name>
        <servlet-class>org.apache.camel.component.servlet.CamelHttpTransportServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet>
        <servlet-name>ServletTwo</servlet-name>
        <servlet-class>org.apache.camel.component.servlet.CamelHttpTransportServlet</servlet-class>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <!-- Camel servlet mapping -->
    <servlet-mapping>
        <servlet-name>ServletOne</servlet-name>
        <url-pattern>/one/*</url-pattern>
    </servlet-mapping>

    <servlet-mapping>
        <servlet-name>ServletTwo</servlet-name>
        <url-pattern>/two/*</url-pattern>
    </servlet-mapping>
</web-app>
````

You can then configure two rest configs to use those two Servlet configs:

````java
@Component
public class MyRouter extends RouteBuilder {

    @Override
    public void configure() {

        restConfiguration("servlet")
                .endpointProperty("servletName", "ServletOne");

        restConfiguration("servlet")
                .endpointProperty("servletName", "ServletTwo");


        rest("/")
                .get().produces("application/json").to("direct:start");

        from("direct:start")
                .setBody(constant("{'message': 'Hello world from deployment one!'}"));
    }

}

````