### Focus point next years
- Wildfly Swarm
- Spring boot
- cri-o runtime

Moving from hawkular to prometues

Windows container support in Kube 1.9. Not yet in OpenShift, long term. Problems with multi-tenant and sdn.

### Service brokers
Service Broker is part of Kubernetes, Service Catalog is UI from OpenShift
- New AWS service broker
- New Ansible broker that allows it to run complex playbook bundles. Include playbooks as part of deployments. Support for functional/it-testing playbooks. Will be hosted in RedHat and available from the Red Hat catalog. Custom made playbooks can be added to our own catalog.
    - e.g. you can include playbooks in your deployments to ease self service deployments
- Templates will be added to the Service Broker Catalog. Ansible will be a new addition to the yaml templates. 
- For end-users, there will be one catalog with button. The developer can provide Ansible 'templates',  standard openshift templates etc etc

--Check for Middleware?

- Auto injection of secrets
- Access/visibility control for Service Catalog; e.g. not everyone has access to everythin

### Dev Experience
- Prometheus metrics will be exposed. Hawkular will be dropped
- Working on long-term metrics support. 
- New minishift and cdk
    - Che will be included as a beta, running on the CDK
- No urgency yet into moving to Atomic as base image; will keep RHEL for official images for now

### New services
- Red Hat OpenShift Messaging
- Fuse Online
- Changes to existing stuff (e.g. EAP/Wildfly) to make it easier to deploy in a container
- Serverless via [Apache OpenWhisk](https://openwhisk.apache.org/)
- Include [Istio](https://istio.io/) capabilities into the cluster
    - Traffic control between pods
    - More API control in inboud

### Networking
- Include the NetworkPolicy kind
- UX for network policies
- Multiple ip ranges; more flexibility and extendibility
- Set cookie names from route def
- All outgoing traffic via one IP, to make firewall config easier
- [Open vSwitch](http://openvswitch.org/) inclusion (OVN)
- New Networking plugins

### Storage
-Local persistant storage (store on local node)
- Additional Cluster support and integration
    - Direct integration for e.g. metrics; can directly store to gluster
- Standardized storage interfacting. 
    - Storage provider should provide driver, instead of current situation where this is defined in openshift

### Security
- Signature checks
- Node security; node Autorizer and Restriction 
    - E.g. Limit access of pod to only config maps on the same node
- Rolebased access control
   - Groups, roles and services
  - Will be the same as the Kubernetes implementation. This will replace the OpenShift variant in time
- FlexVolume, allows to bound a mount in a running pod
- Longer lived API tokens with expire timings
- inclusion of [Grafeas and Kritis](https://grafeas.io/)


### Kubernetes 
.

### Master
- Public pull url for images
- Customer Resource definitions (Exends kube api)
- API aggregation (single API to interact with, will delegate; easy to extend to add thrid party api without changing kube codebase)
- Multi cluster support (delayed..)
    - Working on cluster registry
        - e.g. client queries registry to find out capabilities etc

### Windows
- Images and host
- Needs better docker support in Windows
- Lot of work / tasks still open

### Installation
- New playbooks for etcd migration and scaling
- Made installer modular
    - Can run individual modules in any order needed, skip modules not needed etc
- Changes to ansible core to facilitate smoother installation
- Health checks after install (adhoc playbooks)
- Multi-version upgrades
- ability to skip version
- LTS versions
- Golden images and cluster manifest

- depracating rpm install of OpenShift
    - Moving from RHEL to Atomic installs

- Moving away from Docker to cri-o containers. (Docker support will be phased-out eventually; long term) 
    - cri-o supports running of docker containers
    - Cri-o is lightweight, designed for Kubernetes
    - Runs any OCI/Docker based container
    - November 2018 as targeted date for OpenShift Online
- Build container with Buildah to run in cri-o (OCI)
    - Daemon-less tool for building oci/docker images